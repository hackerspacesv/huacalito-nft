# Huacal NFT 2021

![Huacal Image](./Artwork/huacal_red.svg "Huacal Artwork")

Welcome to the main repository of Huacalito NFT 2021.

In El Salvador is a common tradition that small-business give small
gifts to their clients at the end of the year. This small gifts
usually take the form of small bowls (Huacales) or blankets to
carry tortillas (mantas para las tortillas).

Let's upgrade this tradition and give it a totally new and distruptive
spin-off giving people the ability to gift an unique virtual
huacal!!!

NFTs in their most standard form requires to store the NFT metadata
and the contents it points to traditional centralized platforms
or IPFS.

IPFS requires the user either to seed itselft the content to be able
in the future (In case is not a popular content) or to pay someone
else to seed the content.

Huacalito NFTs solves the storage problem storing the actual art (An SVG
generated from the first search result in google for the keyword "huacal")
to the blockchain itself. This essentially means that all Huacalitos NFTs
reside enterely on the blockchain once they are minted.

Minted huacalitos reside on the Ethereum Ropsten Test Network. Make sure
to change the network before trying to access them or before requesting them.

To add Huacalitos as virtual assets add the following Smart Contract address
in Metamask (Or any other compatible app):

    0xcd3672ce7c54bf71ecbe21a2093a495490a2ae8a

If you want me to Mint a Huacal for you, you can reach me at
[@mxgxw_gamma](https://twitter.com/mxgxw_gamma)

See your Huacal online at [guacalito.org](https://guacalito.org) (Requires Metamask
add-on or browsing it using the Metamask Browser). It only requires the
ID of your minted Huacal.

This (Parody) project is shared under the MIT License. See [LICENSE.txt](./LICENSE.txt)
for more details.
